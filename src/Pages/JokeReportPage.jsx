import authGuard from "../Components/Guards/AuthGuard";
import JokeRecommender from "../Components/JokeRecommender/JokeRecommender";
import JokeReport from "../Components/JokeReport/JokeReport";

function JokeReportPage() {
    return (
      <>
        <h1>JokeReport Page Works</h1>
        <JokeReport></JokeReport>
        <JokeRecommender></JokeRecommender>
      </>
    );
  }
  
  export default authGuard(JokeReportPage);