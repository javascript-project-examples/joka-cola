import logo from './logo.svg';
import './App.css';
import LoginForm from './Components/Login/Login';
import JokeRater from './Components/JokeRater/JokeRater';
import JokeRecommender from './Components/JokeRecommender/JokeRecommender';
import JokeReport from './Components/JokeReport/JokeReport';
import { BrowserRouter, Routes, Route, NavLink } from 'react-router-dom';
import LoginPage from './Pages/LoginPage';
import JokeRatePage from './Pages/JokeRatePage';
import JokeReportPage from './Pages/JokeReportPage';

function App() {

// [x] - Forms Hook
// [x] - Guarding / redirects
// [x] - Programmatic navigation
// [x] - URL params
// [x] - Patch


  return (
    
    <BrowserRouter>
    <NavLink to="/">Home</NavLink>
    <NavLink to="/rate">Rate</NavLink>
    <NavLink to="/report">Report</NavLink>
    <div className="App">
      <Routes>
        <Route path='/' element={<LoginPage/>}/>
        <Route path='/Rate' element={<JokeRatePage/>}/>
        <Route path='/Report' element={<JokeReportPage/>}/>
      </Routes>
    </div>
    </BrowserRouter>
    
    
    
  );
}


export default App;
