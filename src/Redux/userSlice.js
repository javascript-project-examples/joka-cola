import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const getSampleUser = createAsyncThunk("user/getSampleUser",
async()=>{
    const response  = await fetch('https://randomuser.me/api/')
    const userData = await response.json()
    return userData
})

export const userSlice = createSlice({
  name: "user",
  initialState: {
    username: "no user name",
    email: "none",
  },
  reducers: {
    setSample: () => {
      return {
        username: "sample",
        email: "sample@sample.com",
      };
    },
  },
  extraReducers:{
    [getSampleUser.fulfilled]:(state,action)=>{
        console.log(action.payload)
        return action.payload
    }
  }
});

export const {setSample} = userSlice.actions
export default userSlice.reducer