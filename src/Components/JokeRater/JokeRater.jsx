import { useState } from "react";
import JokeDisplayer from "../JokeDisplayer/JokeDisplayer";

function JokeRater() {
  let  [CurrentJoke,setCurrentJoke] = useState({
    error: false,
    category: "Programming",
    type: "twopart",
    setup: ".NET developers are picky when it comes to food.",
    delivery: "They only like chicken NuGet.",
    flags: {
      nsfw: false,
      religious: false,
      political: false,
      racist: false,
      sexist: false,
      explicit: false,
    },
    id: 49,
    safe: true,
    lang: "en",
  });

  function getNextJoke() {
    fetch(
      "https://v2.jokeapi.dev/joke/Any?blacklistFlags=nsfw,religious,political,racist,sexist,explicit&type=twopart"
    )
      .then((response) => response.json())
      .then((jokeData) => setCurrentJoke(jokeData));
  }

  function rateROFL(){
//store as ROFL
getNextJoke()

  }

  function rateMEH(){
    // store MEH
    getNextJoke()

  }

  return (
    <>
      <h1>Jokerater</h1>
      <JokeDisplayer JokeData={CurrentJoke} />
      <button onClick={rateROFL}>🤣</button>
      <button onClick={rateMEH}>😐</button>
    </>
  );
}

export default JokeRater;
