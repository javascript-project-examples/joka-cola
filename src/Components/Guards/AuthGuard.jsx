import { Navigate } from "react-router-dom";

const authGuard = (Component) => (props) => {
  if (localStorage.getItem("username")!==null) {
   return <Component {...props} />;
  } else {
    return <Navigate to="/" />;
  }
};

export default authGuard;
