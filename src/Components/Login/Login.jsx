import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";

function LoginForm() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const navigate = useNavigate()

  function onSubmit(data) {
    resetUser(1)
    localStorage.setItem("username", data.username)
    navigate('/rate')
  }

  function resetUser(id){
    fetch(`https://thoughtful-vagabond-fibre.glitch.me/users/${id}`, {
        method: 'PATCH', // NB: Set method to PATCH
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            // Provide new translations to add to user with id 1
            username: "reset"
        })
    })
    .then(response => {
      if (!response.ok) {
        throw new Error('Could not update translations history')
      }
      return response.json()
    })
    .then(updatedUser => {
      // updatedUser is the user with the Patched data
    })
    .catch(error => {
    })

  }

  return (
    <>
      <h1>Login form works</h1>
      <form onSubmit={handleSubmit(onSubmit)}>
        <input {...register("username",{required:true})} type="text" />
        <button type="submit">Login</button>
      </form>
    </>
  );
}

export default LoginForm;
